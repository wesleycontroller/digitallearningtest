import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BaseService } from 'src/app/services/base.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  formConfig: FormGroup;
  constructor(
    private fb: FormBuilder,
    private base: BaseService,
    private router: Router
  ) { }

  ngOnInit() {
    this.formConfig = this.fb.group({
      login: ['', [Validators.minLength(6), Validators.maxLength(16)]],
      password: ['', [Validators.minLength(6), Validators.maxLength(16)]]
    })

  }
  submmit() {
    console.log(this.formConfig)
    this.base.setDado('Token', this.formConfig.value).subscribe((res: any) => {
      console.log(res)
      localStorage.setItem('access_token', res.data.access_token)
      localStorage.setItem('refresh_token', res.data.refresh_token)

      this.router.navigate(['/list']);

    }, erro => {
      //console.log(erro)
    })
  }
  register(){
    this.router.navigate(['/register']);
  }

}
