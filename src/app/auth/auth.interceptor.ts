import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpErrorResponse, HttpXsrfTokenExtractor, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { NbToastrService } from '@nebular/theme';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private router: Router,
    private toastrService: NbToastrService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let token = localStorage.getItem('access_token')

    let requestToForward = req

    if (token) {
      let tok = token
      requestToForward = req.clone({
        headers: req.headers.set('Authorization', 'Bearer ' + tok),
      });
    } else {
      localStorage.clear()
      this.router.navigate(['/auth/login']);
    }
    return next.handle(requestToForward).pipe(tap(() => { },
      (err: any) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status !== 401) {
            if (err.status == 400) {
              console.log(err)
              this.toastrService.danger(err.error.message, "Erro")
            }
            if (err.status == 500) {

              this.toastrService.danger("a aplicação não identificou como válida sua requisição", "Erro 500")

            }
            if (err.status == 403) {

              this.toastrService.danger("você não tem permissão para executar esta ação", "Erro")

            }
            return;
          } else {
            localStorage.clear()
            this.router.navigate(['/auth/login']);
          }

        }
      }));

  }

}
