import { Component, OnInit } from '@angular/core';
import { BaseService } from 'src/app/services/base.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  settings: any = {}
  data: any = []

  constructor(private base: BaseService,     private router: Router
    ) { 
    this.settings = {
      noDataMessage: "Não foram encontrados dados",
      mode: "external",
      columns:  {
        fullName: {
          title: 'Nome'
        },
        login: {
          title: 'login'
        },
        email: {
          title: 'Email'
        },
        cpf: {
          title: 'CPF'
        }
      },
      actions: false,
      

    };
  
  }

  ngOnInit() {
    console.log("chama")
    this.base.getDados('List').subscribe((res:any)=>{
      this.data = res.data
    })

  }
  exit(){
    localStorage.clear()
    this.router.navigate(['/login']);
  }

}
