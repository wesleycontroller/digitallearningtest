import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { ListComponent } from './pages/list/list.component';
import { RegisterComponent } from './auth/register/register.component';


const routes: Routes = [
  { path: 'list', component: ListComponent },
  {
    path: 'register',
    component: RegisterComponent,
    data: { title: 'Registre Novo' }
  },
  { path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  { path: '**', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
